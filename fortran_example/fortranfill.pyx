import numpy as np

cdef extern:
    void fill_(int *, double *)

def fill(double[::1, :] a):
    cdef int n
                
    n = a.shape[0]
    
    fill_(&n, &a[0, 0])

    
