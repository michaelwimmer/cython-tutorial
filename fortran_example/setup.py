from setuptools import setup, Extension  
from Cython.Build import cythonize

ext_modules = cythonize([
    Extension("fortranfill", ["fortranfill.pyx"],
              extra_objects=['fill.o'],
              libraries=['gfortran'])
    ])


setup(
    name='GSL wrapper',
    ext_modules=ext_modules,
    zip_safe=False,
)
