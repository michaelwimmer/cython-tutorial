from setuptools import setup, Extension
from Cython.Build import cythonize

ext_modules = cythonize([
    Extension("gslint", ["gslint.pyx"],
              libraries=["gsl"])
    ])


setup(
    name='GSL wrapper',
    ext_modules=ext_modules,
    zip_safe=False,
)
