# Specify all the functions we would like to use from gsl

cdef extern from "gsl/gsl_integration.h":
    
    ctypedef struct gsl_integration_cquad_workspace:
        pass

    ctypedef struct gsl_function: 
        double (* function) (double x, void * params)
        void * params
        
    gsl_integration_cquad_workspace *gsl_integration_cquad_workspace_alloc(size_t n)
    void gsl_integration_cquad_workspace_free(gsl_integration_cquad_workspace * w)
    int gsl_integration_cquad(const gsl_function * f, double a, double b, double epsabs,
                              double epsrel, gsl_integration_cquad_workspace * workspace,
                              double * result, double * abserr, size_t * nevals)
    

cdef double c_func(double x, void * params):
    return (<object>params)(x)
    
    
def gsl_cquad(f, a, b, epsabs=1e-4, epsrel=1e-3):
    """Integrates f over the interval [a, b] using the CQUAD method from gsl.
    
    Parameters
    ----------
    f:
        function to be integrated over
    a, b:
        integration interval
    epsabs, epsrel:
        desired absolute and relative accuracies (check GSL documentation)
    
    Returns
    -------
    result:
        value of the integral
    abserr:
        error of the computed values
    neval:
         number of function evaluations
    """
    
    cdef gsl_function func
    cdef double result, abserr
    cdef size_t nevals
    cdef int retval
    cdef gsl_integration_cquad_workspace *workspace
    
    func.function = c_func
    func.params = <void *>f
    
    workspace = gsl_integration_cquad_workspace_alloc(1000)
    
    retval = gsl_integration_cquad(&func, <int>a, <int>b, <double>epsabs, <double>epsrel,
                                   workspace, &result, &abserr, &nevals)
    
    gsl_integration_cquad_workspace_free(workspace)
    
    return result, abserr, nevals
    

